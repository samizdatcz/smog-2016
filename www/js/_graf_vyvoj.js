var colors = ['#FF4136', '#0074D9', '#B10DC9', '#3D9970', '#FF851B', '#FFDC00','#001F3F', '#2ECC40']

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#graf_vyvoj').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Porušování limitů PM10'
        },
        subtitle: {
            text: ''
        },
        xAxis: {},
        yAxis: {
            title: {
                text: 'Podíl stanic s překročenými limity (%)'
            }
        },
        tooltip: {
            formatter: function() {
                return 'V roce ' + this.x + ' byl překročen denní limit PM10 na <b>' + this.y + ' % stanic</b>'
            },
            crosshairs: true
        },
        exporting: {
            enabled: false
        },
        credits: {
            href : 'http://portal.chmi.cz/files/portal/docs/uoco/isko/grafroc/grafroc_CZ.html',
            text : 'Zdroj: ČHMÚ (2002-2015), vlastní výpočet (2016)'
        },
        plotOptions: {
        },
        series: [{
            name: 'PM10',
            data: [{x:2002, y:47.1}, {x:2003, y:73.9}, {x:2004, y:58.5}, {x:2005, y:67.9}, {x:2006, y:63.5}, {x:2007, y:35.3}, {x:2008, y:30.7}, {x:2009, y:33.8}, {x:2010, y:52.5}, {x:2011, y:56.7}, {x:2012, y:41.7}, {x:2013, y:32.6}, {x:2014, y:42.9}, {x:2015, y:23.4}, {x:2016, y:22.6}],
            color: colors[0]
        }]
    });
});