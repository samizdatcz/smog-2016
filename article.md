---
title: "Koncentrace prachu na čtvrtině stanic byla loni vyšší, než povoluje zákon. Podívejte se, jak se dýchá u vás"
perex: "V posledních patnácti letech ubylo míst, kde dochází k porušování limitů. S těmi zbývajícími – hlavně Ostravskem – si ovšem stát neví rady."
description: "V posledních patnácti letech ubylo míst, kde dochází k porušování limitů. S těmi zbývajícími – hlavně Ostravskem – si ovšem stát neví rady."
authors: ["Jan Boček", "Jan Cibulka"]
published: "24. ledna 2017"
coverimg: https://interaktivni.rozhlas.cz/data/smog-2016/www/media/cover.jpg
coverimg_note: "Foto <a href='https://www.flickr.com/photos/un_photo/5471608132/in/photolist-9kvqtf-8UR1Uy-e9ndhb-pifkSL-qfeprn-AZitdc-BUqhwo-mLy5q8-mHNyt3-o1zbQ6-bEx5zq-mLKeDY-wxYDHW-pitAuK-eF4jxP-9S51Jx-gut2X8-jS2P2o-4FRY7h-dWsubT-m2wmSD-91cW17-7m6NYk-KyS9h-7dZBnA-jS1CrM-azgaGC-3RHpJ-na5y1o-4FMNkz-gcjZkJ-4FMNqc-nrTUVk-amCztB-j83th9-mjsSN6-aQbM7z-ntkKWx-ehNsHV-mHLhVJ-iC2RW2-72uaqF-o8232G-6SUoPN-pJRssW-9dRquT-r1nJB-nW1aDy-4FVBdp-9BSs7X'>United Nations Photo</a> (CC BY-NC-ND 2.0)"
socialimg: https://interaktivni.rozhlas.cz/data/smog-2016/www/media/socialimg.jpg
url: "smog-2016"
libraries: ["https://unpkg.com/d3@3.5.17/d3.js", jquery, "https://unpkg.com/leaflet@1.0.3/dist/leaflet.js", highcharts]
styles: ["./styl/map.css", "https://unpkg.com/leaflet@1.0.3/dist/leaflet.css"]
recommended:
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/znecisteni-vzduchu-neklesa-muzou-za-to-hlavne-kamna-na-uhli-rika-ministerstvo--1509118
    title: Znečištění vzduchu neklesá. Můžou za to hlavně kamna na uhlí, říká ministerstvo
    perex:  Pokud se nepovede situaci řešit, hrozí Česku žaloba k Soudnímu dvoru EU.
    image: http://media.rozhlas.cz/_obrazek/2237723--znecisteni-ilustracni-foto--1-300x198p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/1570624
    title: Znečištění vzduchu hodinu po hodině: Podívejte se, co dýcháte
    perex: Kdy je vzduch čistý, a kdy je lepší se zavřít doma a nevycházet?
    image: http://media.rozhlas.cz/_obrazek/2789005--smog-nad-bohuminem--1-300x225p0.jpeg
  - link: https://interaktivni.rozhlas.cz/horko-ve-mestech/
    title: Mapa pražského horka: Stromy ochladí okolí o několik stupňů
    perex: Hlavní město hledá cesty, jak se vypořádat s tropickými vedry.
    image: https://interaktivni.rozhlas.cz/horko-ve-mestech/media/socimg.png
---

V Česku dlouhodobě dochází k překračování prachových limitů. Minulý rok nebyl výjimka: denní limity pro prachové částice do 10 mikrometrů (PM10) byly porušeny na 23 procentech měřicích stanic.

Vyplývá to z aktuálních dat o znečištění vzduchu, která [ČHMÚ zveřejňuje na svém webu](http://portal.chmi.cz/files/portal/docs/uoco/web_generator/actual_hour_data_CZ.html). Český rozhlas tato data každou hodinu ukládá, takže lze sledovat znečištění vzduchu v průběhu celého minulého roku.

Podle zákona smí průměrná denní koncentrace částic PM10 přesáhnout 50 mikrogramů na krychlový metr maximálně 35krát ročně. Mapa ukazuje, které stanice se do limitu vešly (modré) a kde se zákonný limit dodržet nepodařilo (červené). Velikost kruhu odpovídá počtu překročení limitu. Spodní část pak ukazuje všechny dny v roce, červené body označují dny s překročeným limitem.

<aside class="big">
  <div id="map"></div>
  <div id="graf"></div>
</aside>

Rekordmanem jsou – podobně jako například u [koncentrace rakovinotvorného benzo(a)pyrenu](http://www.rozhlas.cz/zpravy/data/_zprava/znecisteni-vzduchu-neklesa-muzou-za-to-hlavne-kamna-na-uhli-rika-ministerstvo--1509118) – ostravské Radvanice. Koncentrace prachových částic zde překročila limit třikrát častěji, než povoluje zákon.

<div data-bso=1></div>

## MŽP: Už příští rok bude smogu míň

Řešení nepříznivé situace koordinuje ministerstvo životního prostředí, hlavním nástrojem jsou [regionální programy zlepšování kvality ovzduší](http://www.mzp.cz/cz/programy_zlepsovani_kvality_ovzdusi).

Podle právnické organizace Frank Bold jsou ovšem plány ministerstva nedostatečné.

„Programy zlepšování kvality ovzduší sice navrhují řadu opatření, ale už nespecifikují, která z nich se budou skutečně realizovat,“ vysvětluje právnička Kristína Šabová z této organizace. „Například pro zlepšení ovzduší v Brně nebo Praze jsou klíčová opatření v dopravě, bez kterých ke zlepšení nedojde. U nich to ale na rychlé řešení nevypadá.“

Ve čtyřech nejvíce zatížených oblastech – ostravské aglomeraci, Ústecku, Brně a Praze – proto loni lokální iniciativy s podporou Franka Bolda podaly na postup ministerstva žalobu. Všechny žaloby projednával Městský soud v Praze; první tři zamítl, jednání o pražském programu bylo na návrh stěžovatelů přerušeno.

„Soudy dosud rozhodovaly velmi formálně,“ tvrdí Šabová. „Stačilo jim, že programy zlepšování kvality ovzduší existují a že mají všechny přílohy, které mít mají. Posouzení, zda programy obsahují dostatečně vymezená a účinná opatření, se nevěnovaly. Tím mimo jiné ignorují judikaturu evropského soudu, která naopak požaduje posouzení skutečné váhy opatření.“

„Proto jsme také u všech tří za navrhovatele podali kasační stížnost k Nejvyššímu správnímu soudu,“ doplňuje Šabová. „Jakmile rozhodne, na základě výsledku bude pokračovat projednávání také v Praze.“

Ministerstvo životního prostředí je naopak s rozhodnutím krajských soudů spokojeno.

„Navrhovaná opatření jsou dostatečná,“ říká Petra Roubíčková, tisková mluvčí ministerstva. „Pokud se je povede realizovat, někdy v roce 2018 nebo 2019 by mělo dojít ke zlepšení. Mírně se to zlepšuje už teď.“

## Čistých míst přibývá, Ostrava zůstává černá

Trend podílu stanic, na kterých došlo k porušení zákonných limitů, jí dává za pravdu. Poslední dva roky jsou nejčistší nejméně od roku 2002.

<aside class="big">
  <div id="graf_vyvoj"></div>
</aside>

Za vysokou variabilitou časové řady jsou podle materiálů ministerstva životního prostředí hlavně změny v počasí. Jedním z hlavních zdrojů prachových částic jsou podle nich kamna a kotle, v některých oblastech jsou zdrojem až devadesáti procent prachu. Mrazivé počasí proto smogovou situaci zhoršuje. Podobně nepříznivý vliv mají také podzimní inverze.

Ostravsko si na rozdíl od zbytku republiky v posledních letech nepolepšilo. Prachový limit každoročně překračuje většina tamních stanic. V počtu dní, kdy byly překročeny limity PM10, letos stanice z ostravského regionu obsadily prvních patnáct míst.

„Ostravsko je specifické tím, jak moc se liší zdroje znečištění v různých lokalitách,“ vysvětluje obtížné řešení ostravské situace tisková mluvčí ministerstva Petra Roubíčková. „Obecně lze říct, že asi třicet procent znečištění má na svědomí doprava a třicet procent lokální topeniště. V každém městě nebo městské části je ale kombinace zdrojů znečištění úplně jiná a vyžaduje jiná opatření. K tomu se přidává znečištění z polské strany.“

## Očekávaná délka života na Ústecku i Ostravsku je o rok až dva podprůměrná

Prachové částice jsou přitom velkým zdrojem zdravotních problémů. Přiznává to i ministerstvo životního prostředí v [Národním programu snižování emisí](http://www.mzp.cz/cz/narodni_program_snizovani_emisi). Částice PM10, které měří většina stanic, navíc nejsou jediným rizikem: ještě problematičtější jsou drobnější PM2,5, které se ovšem měří jen na malé části stanic, a zřejmě také prachové částice do jednoho mikrometru PM1; ty se neměří téměř nikde a v české legislativě pro ně neexistují limity. Tvoří přitom víc než polovinu všech prachových částic.

„Suspendované částice PM2.5 představují, vzhledem ke své schopnosti pronikat hlouběji do plicních sklípků, výrazně vyšší zdravotní riziko než PM10,“ píše se ve zmíněném programu. „Podle odhadů mají tyto částice sedmiprocentní podíl na všech předčasných úmrtích v Česku. Zkracují naději dožití o 8,6 měsíce u mužů a 8,4 měsíce u žen.“

Obě kategorie pak mají na svědomí zhoršení funkce plic, vyšší nemocnost dýchací soustavy, chronické záněty průdušek, nemoci srdce a cév a zřejmě i rakovinu plic.

Problémy s prachovými částicemi nicméně nejsou českou specialitou; podle Evropské agentury pro životní prostředí denně dýchá vzduch s překročenými limity PM10 třetina evropské městské populace. Dodržování limitů pak vymáhá evropský soud – během února se čeká výsledek [soudní žaloby na Belgii a Bulharsko](http://europa.eu/rapid/press-release_IP-15-5197_en.htm).

O kolik prachové částice zkrátí život, ilustruje i ukazatel [naděje dožití při narození v jednotlivých krajích](https://www.czso.cz/documents/10180/32846217/130055161004.pdf/90dd1343-e674-4ee8-b00e-5a0e49314e86?redirect=https%3A%2F%2Fwww.czso.cz%2Fcsu%2Fxj%2Fstredni-delka-zivota-se-zvysuje%3Fp_p_id%3D3%26p_p_lifecycle%3D0%26p_p_state%3Dmaximized%26p_p_mode%3Dview%26_3_groupId%3D0%26_3_keywords%3Do%25C4%258Dek%25C3%25A1van%25C3%25A1%2520d%25C3%25A9lka%2Bdo%25C5%25BEit%25C3%25AD%26_3_struts_action%3D%252Fsearch%252Fsearch%26_3_redirect%3D%252Fweb%252Fxj%252Fstranka). Ta je u mužů i u žen shodně nejnižší na Ústecku, následovaném Moravskoslezským krajem. Očekávaná délka života je u mužů i žen o rok až dva pod celorepublikovým průměrem. Znečištění vzduchu je pouze jedním ze zdrojů tohoto deficitu, zřejmě ale dost podstatným.

<aside class="big">
  <iframe src="https://interaktivni.rozhlas.cz/data/smog-2016/www/charts/doziti.htm" width="100%" height="600" scrolling="no" frameborder="0"></iframe>
</aside>